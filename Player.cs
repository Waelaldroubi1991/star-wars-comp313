﻿//Wael Aldroubi
//300456658

/*player movement and control*/

/*libraries needed for this script*/
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    //variable to control new position.
    private Vector2 targetPos;
    //variable to control movement to the right and left.
    public float Xincrement;
    //variable to control movement to the up and down.
    public float Yincrement;
    //variable to control the speed of the charctar.
    public float speed;
    //variables to control the area of screen so player won't get out of the screen.
    public float maxwidth;
    public float minwidth;
    public float maxheight;
    public float minheight;
    //variable to display player's health.
    public Text healthDisplay;
    //variable to control game over scene when player's health is 0.
    public GameObject gameOver;
    //Default variable to set player's health to 10 will decrese when have collision with obstacles.
    public int health = 10;

    private void Update()
    {   //to view the health of player
        healthDisplay.text = health.ToString();
        //when player health reach 0
        if (health <= 0)
        {   //game will load the game over scene.
            gameOver.SetActive(true);
            //will destroy player
            Destroy(gameObject);
        }
        transform.position = Vector2.MoveTowards(transform.position, targetPos, speed * Time.deltaTime);
        if (Input.GetKeyDown(KeyCode.RightArrow) && transform.position.x < maxwidth)
        {
            //if player press the right arrow then the charcter will move depending on the target variable to the right.
            targetPos = new Vector2(transform.position.x + Xincrement, transform.position.y);
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow) && transform.position.x > minwidth)
        {
            //if player press the left arrow then the charcter will move depending on the target variable to the left.
            targetPos = new Vector2(transform.position.x - Xincrement, transform.position.y);
        }
        else if (Input.GetKeyDown(KeyCode.UpArrow) && transform.position.y < maxheight)
        {
            //if player press the up arrow then the charcter will move depending on the target variable up.
            targetPos = new Vector2(transform.position.x, transform.position.y + Yincrement);
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow) && transform.position.y > minheight)
        {
            //if player press the down arrow then the charcter will move depending on the target variable down.
            targetPos = new Vector2(transform.position.x, transform.position.y - Yincrement);
        }
    }
}