﻿//Wael Aldroubi
//300456658

/*Obstacles movement and control*/

/*libraries needed for this script*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Restart : MonoBehaviour {
	// Update is called once per frame
	void Update ()
    {   //when game is over the game over scene will be loaded from player script.
        //player can press R to restart the game and play again.
	    if (Input.GetKeyDown(KeyCode.R))
        {   //will load the game scene again.
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
	}
}
