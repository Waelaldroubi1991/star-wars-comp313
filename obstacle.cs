﻿//Wael Aldroubi
//300456658

/*Obstacles movement and control*/

/*libraries needed for this script*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class obstacle : MonoBehaviour
{   //each obstacle will damage the player 1 point when the collieder collision happen.
    public int damage = 1;
    //to control the speed of the obstacle.
    public float speed;

    // Update is called once per frame
    void Update()
    {   //the movement of the obstacle.
        transform.Translate(Vector2.left * speed * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {   //to find the collieder of player.
        if (other.CompareTag("Player"))
        {
            //player take damage
            other.GetComponent<Player>().health -= damage;
            Debug.Log(other.GetComponent<Player>().health);
            //to destroy the obstacle after collsion with player.
            Destroy(gameObject);
        }
    }
}
