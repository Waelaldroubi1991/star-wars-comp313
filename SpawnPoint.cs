﻿//Wael Aldroubi
//300456658

/*Obstacles movement and control*/

/*libraries needed for this script*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour {
    //to store obstacles in the spawner object to load them.
    public GameObject obstacle;
	//this function will spawn obstacles from the spawner object to the side of the screen.
	void Start () {
        Instantiate(obstacle, transform.position, Quaternion.identity);
	}
}
