﻿//Wael Aldroubi
//300456658

/*Obstacles movement and control*/

/*libraries needed for this script*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroyer : MonoBehaviour {
    //variable to control obstacles life time.
    public float lifetime;
	// Use this for initialization
    //function to destroy obstacle object after leaving the game scene (set with time)
    //so it won't consume the memory.
	private void Start ()
    {
        Destroy(gameObject, lifetime);
	}
	
	
}
