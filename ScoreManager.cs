﻿//Wael Aldroubi
//300456658

/*Obstacles movement and control*/

/*libraries needed for this script*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {
    //variable to store score.
    public int score;
    //variable to view score.
    public Text ScoreDisplay;
    //to view score of player
    private void Update()
    {   
        ScoreDisplay.text = score.ToString();
    }
    //when obstacles have collision with the score collieder the number of score will increase.
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("obstacle"))
        {
            score++;
            Debug.Log(score);
        }
    }
}
