﻿//Wael Aldroubi
//300456658

/*Obstacles spawner and pattern control*/

/*libraries needed for this script*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {
    //to connect the 3 pattern to the obstacle object
    //pattern are (up&down, Middle&up. Middle&down)
    public GameObject[] obstaclePatterns;
    //to control time when the obstacles patterns start to spawn and then next one
    //and time will decrease to make game harder.
    private float timeBtwSpawn;
    public float startTimeBtwSpawn;
    public float decreaseTime;
    public float minTime = 0.65f;
	
	// Update is called once per frame
	private void Update ()
    {
	    if (timeBtwSpawn <= 0)
        {   //to make spawn obstacle patterns Random.
            int rand = Random.Range(0, obstaclePatterns.Length);
            Instantiate(obstaclePatterns[rand], transform.position, Quaternion.identity);
            timeBtwSpawn = startTimeBtwSpawn;
            if (startTimeBtwSpawn > minTime)
            {   //to decrease the time between current spawn and the next one
                startTimeBtwSpawn -= decreaseTime;
            }
        }
        else
        {
            timeBtwSpawn -= Time.deltaTime;
        }
	}
}
